#ifndef LAB2_SEQUENCE_H
#define LAB2_SEQUENCE_H

template <typename T>
class Sequence {
  public:
    // Decomposition
    virtual T GetFirst() const = 0;
    virtual T GetLast() const = 0;
    virtual T Get(int index) const = 0;
    virtual Sequence<T>* GetSubsequence(int startIndex, int endIndex) const = 0;
    virtual int GetLength() const = 0;
    // Operations
    virtual void Append(T item) = 0;
    virtual void Prepend(T item) = 0;
    virtual void InsertAt(T item, int index) = 0;
    virtual Sequence<T>* Concat(Sequence<T>* list) = 0;
    // Destructor
    virtual ~Sequence() {}
};

#endif
