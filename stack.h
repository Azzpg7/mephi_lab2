
#ifndef DEQUEUE_H
#define DEQUEUE_H

#include "arraysequence.h"
#include "linkedlistsequence.h"
#include "sequence.h"

template <typename T>
class Stack {
 protected:
  shared_ptr<Sequence<T>> value;
  int size;

 public:
  T GetLast() const { return value->GetLast(); }
  void SetLast(T item) {
    value->Append(item);
    size++;
  }
  void RemoveLast() {
    value = shared_ptr<Sequence<T>>(value->GetSubsequence(0, size - 2));
    size--;
  }
  virtual ~Stack() {}
};

template <typename T>
class ArrayStack : public Stack<T> {
 public:
  ArrayStack() {
    this->value = make_shared<ArraySequence<T>>();
    this->size = this->value->GetLength();
  }
  ArrayStack(T *array, int count) {
    this->value = make_shared<ArraySequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

template <typename T>
class LinkedListStack : public Stack<T> {
 public:
  LinkedListStack() {
    this->value = make_shared<LinkedListSequence<T>>();
    this->size = this->value->GetLength();
  }
  LinkedListStack(T *array, int count) {
    this->value = make_shared<LinkedListSequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

#endif
