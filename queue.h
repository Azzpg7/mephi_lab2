
#ifndef QUEUE_H
#define QUEUE_H

#include "arraysequence.h"
#include "linkedlistsequence.h"
#include "sequence.h"

template <typename T>
class Queue {
 protected:
  shared_ptr<Sequence<T>> value;
  int size;

 public:
  T GetFirst() const { return value->Get(0); }
  void SetLast(T item) {
    value->Append(item);
    size++;
  }
  void RemoveFirst() {
    value = shared_ptr<Sequence<T>>(value->GetSubsequence(1, size - 1));
    size--;
  }
  virtual ~Queue() {}
};

template <typename T>
class ArrayQueue : public Queue<T> {
 public:
  ArrayQueue() {
    this->value = make_shared<ArraySequence<T>>();
    this->size = this->value->GetLength();
  }
  ArrayQueue(T *array, int count) {
    this->value = make_shared<ArraySequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

template <typename T>
class LinkedListQueue : public Queue<T> {
 public:
  LinkedListQueue() {
    this->value = make_shared<LinkedListSequence<T>>();
    this->size = this->value->GetLength();
  }
  LinkedListQueue(T *array, int count) {
    this->value = make_shared<LinkedListSequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

#endif
