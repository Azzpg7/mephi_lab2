#ifndef LAB2_CSTRING_H
#define LAB2_CSTRING_H

#include <cstring>

#include "arraysequence.h"
#include "linkedlistsequence.h"

template <typename T>
class CString {
  protected:
    Sequence<T>* value;
    int len;
    long long* hash;
    long long* CalcHash();

    virtual Sequence<T>* GetValue() const = 0;

  public:
    // Decomposition
    int GetLength() const { return this->len; }
    T Get(int index) const { return value->Get(index); }
    // Operations
    int Find(const CString<T>* cstring) const;
    long long GetHash(int from, int to) const;
    CString<T>* Concat(CString<T>* cstring);
    virtual CString<T>* GetSubstring(int from, int to) const = 0;
    virtual CString<T>* Replace(CString<T>* old, CString<T>* nw) = 0;
    // Destructor
    virtual ~CString();
};

template <typename T>
long long* CString<T>::CalcHash() {
    int len = this->value->GetLength();
    long long* h = new long long[len];
    for (int i = 0; i < len; i++) {
        h[i] = 0;
    }
    T tmp1 = this->value->Get(0);
    memcpy(h, &tmp1, min(sizeof(T), sizeof(long long)));
    for (int i = 1; i < len; i++) {
        long long cur = 0;
        tmp1 = this->value->Get(i);
        memcpy(&cur, &tmp1, min(sizeof(T), sizeof(long long)));
        h[i] = (h[i - 1] * 127 + cur) % 1000000007;
    }
    return h;
}

template <typename T>
CString<T>* CString<T>::Concat(CString<T>* cstring) {
    this->value = this->value->Concat(cstring->value);
    this->len = this->value->GetLength();
    return this;
}

template <typename T>
CString<T>::~CString() {}

template <typename T>
int CString<T>::Find(const CString<T>* cstring) const {
    int ln = cstring->len;
    long long sampleHash = cstring->GetHash(0, ln - 1);
    for (int i = 0; i < this->len - ln + 1; i++) {
        if (this->GetHash(i, i + ln - 1) == sampleHash) {
            return i;
        }
    }
    return -1;
}

template <typename T>
long long CString<T>::GetHash(int from, int to) const {
    if (from > to || from < 0 || to > this->len) {
        throw out_of_range("index out of range");
    }
    long long big = this->hash[to];
    long long little = 0;
    long long ppow = 1;
    long long a = 127;
    int n = to - from + 1;
    while (n) {
        if (n & 1) {
            ppow = ppow * a % 1000000007;
        }
        a = a * a % 1000000007;
        n >>= 1;
    }
    if (from > 0) {
        little = (this->hash[from - 1] * ppow) % 1000000007;
    }

    return (big - little + 1000000007) % 1000000007;
}

#endif /* LAB2_CSTRING_H */
