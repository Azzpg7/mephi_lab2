#ifndef LAB2_LISTCSTRING_H
#define LAB2_LISTCSTRING_H

#include "cstring.h"
#include "linkedlistsequence.h"

template <typename T>
class ListString : public CString<T> {
  protected:
    Sequence<T>* GetValue() const override;

  public:
    // Constructors
    ListString(T* array, int count);
    ListString(Sequence<T>* sequence);
    // Operations
    CString<T>* GetSubstring(int from, int to) const override;
    CString<T>* Replace(CString<T>* old, CString<T>* nw) override;
    // Destructor
    ~ListString() {
        delete[] this->hash;
        delete this->value;
    }
};

template <typename T>
Sequence<T>* ListString<T>::GetValue() const {
    return this->value;
}

template <typename T>
ListString<T>::ListString(T* array, int count) {
    LinkedListSequence<T>* sequence = new LinkedListSequence<T>(array, count);
    this->value = sequence;
    this->len = this->GetValue()->GetLength();
    this->hash = this->CalcHash();
}

template <typename T>
ListString<T>::ListString(Sequence<T>* sequence) {
    this->value = new LinkedListSequence<T>(sequence);
    this->len = this->value->GetLength();
    this->hash = this->CalcHash();
}

template <typename T>
CString<T>* ListString<T>::GetSubstring(int from, int to) const {
    Sequence<T>* sequence = this->value->GetSubsequence(from, to);
    int len = sequence->GetLength();
    T* array = new T[len];
    for (int i = 0; i < len; i++) {
        array[i] = sequence->Get(i);
    }
    delete sequence;
    ListString<T>* substring = new ListString<T>(array, len);
    delete[] array;
    return substring;
}

template <typename T>
CString<T>* ListString<T>::Replace(CString<T>* old, CString<T>* nw) {
    int oldLen = old->GetLength();
    long long sampleHash = old->GetHash(0, oldLen - 1);
    int index = -1;
    for (int i = 0; i < this->len - oldLen + 1; i++) {
        if (this->GetHash(i, i + oldLen - 1) == sampleHash) {
            index = i;
            break;
        }
    }
    if (index == -1) {
        return this;
    }
    ListString<T>* newString = nullptr;
    if (index > 0) {
        newString = (ListString<T>*)this->GetSubstring(0, index - 1);
        newString->Concat(nw);
    } else {
        newString = (ListString<T>*)nw->GetSubstring(0, nw->GetLength() - 1);
    }
    if (index + oldLen < this->len) {
        ListString<T>* rstring =
            (ListString<T>*)this->GetSubstring(index + oldLen, this->len - 1);
        newString->Concat(rstring);
        delete rstring;
    }
    return newString;
}

#endif
