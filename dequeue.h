#ifndef DEQUEUE_H
#define DEQUEUE_H

#include "arraysequence.h"
#include "linkedlistsequence.h"
#include "sequence.h"

template <typename T>
class Dequeue {
 protected:
  shared_ptr<Sequence<T>> value;
  int size;

 public:
  T GetFirst() const { return value->Get(0); }
  void SetFirst(T item) {
    value->Prepend(item);
    size++;
  }
  T GetLast() const { return value->GetLast(); }
  void SetLast(T item) {
    value->Append(item);
    size++;
  }
  void RemoveFirst() {
    value = shared_ptr<Sequence<T>>(value->GetSubsequence(1, size - 1));
    size--;
  }
  void RemoveLast() {
    value = shared_ptr<Sequence<T>>(value->GetSubsequence(0, size - 2));
    size--;
  }
  virtual ~Dequeue() {}
};

template <typename T>
class ArrayDequeue : public Dequeue<T> {
 public:
  ArrayDequeue() {
    this->value = make_shared<ArraySequence<T>>();
    this->size = this->value->GetLength();
  }
  ArrayDequeue(T *array, int count) {
    this->value = make_shared<ArraySequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

template <typename T>
class LinkedListDequeue : public Dequeue<T> {
 public:
  LinkedListDequeue() {
    this->value = make_shared<LinkedListSequence<T>>();
    this->size = this->value->GetLength();
  }
  LinkedListDequeue(T *array, int count) {
    this->value = make_shared<LinkedListSequence<T>>(array, count);
    this->size = this->value->GetLength();
  }
};

#endif
