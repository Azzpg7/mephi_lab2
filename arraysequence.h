#ifndef LAB2_ARRAYSEQUENCE_H
#define LAB2_ARRAYSEQUENCE_H

#include "dynamicarray.h"
#include "sequence.h"

template <typename T>
class ArraySequence : public Sequence<T> {
  private:
    DynamicArray<T>* items;

  public:
    // Constructors
    ArraySequence() { this->items = new DynamicArray<T>(int(0)); }
    ArraySequence(T* items, int count);
    ArraySequence(const DynamicArray<T>& dynamicArray);
    // Decomposition
    int GetLength() const override { return this->items->GetSize(); }
    T GetLast() const override;
    T GetFirst() const override { return this->items->Get(0); }
    T Get(int index) const override { return this->items->Get(index); }
    void Append(T item) override;
    // Operations
    void Prepend(T item) override;
    void InsertAt(T item, int index) override;
    ArraySequence<T>* Concat(Sequence<T>* list) override;
    Sequence<T>* GetSubsequence(int startIndex, int endIndex) const override;
    // Destructor
    ~ArraySequence() { delete items; }
};

template <typename T>
ArraySequence<T>::ArraySequence(T* items, int count) {
    this->items = new DynamicArray<T>(items, count);
}

template <typename T>
ArraySequence<T>::ArraySequence(const DynamicArray<T>& dynamicArray) {
    this->items = new DynamicArray<T>(dynamicArray);
}

template <typename T>
void ArraySequence<T>::Append(T item) {
    this->items->Resize(this->items->GetSize() + 1);
    this->items->Set(this->items->GetSize() - 1, item);
}

template <typename T>
void ArraySequence<T>::Prepend(T item) {
    this->items->Resize(this->items->GetSize() + 1);
    for (int i = this->items->GetSize() - 1; i > 0; i--) {
        this->items->Set(i, this->items->Get(i - 1));
    }
    this->items->Set(0, item);
}

template <typename T>
void ArraySequence<T>::InsertAt(T item, int index) {
    this->items->Resize(this->items->GetSize() + 1);
    for (int i = this->items->GetSize() - 1; i > index; i--) {
        this->items->Set(i, this->items->Get(i - 1));
    }
    this->items->Set(index, item);
}

template <typename T>
T ArraySequence<T>::GetLast() const {
    return this->items->Get(items->GetSize() - 1);
}

template <typename T>
ArraySequence<T>* ArraySequence<T>::Concat(Sequence<T>* list) {
    int oldSize = this->items->GetSize();
    this->items->Resize(oldSize + list->GetLength());
    for (int i = 0; i < list->GetLength(); i++) {
        this->items->Set(oldSize + i, list->Get(i));
    }
    return this;
}

template <typename T>
Sequence<T>* ArraySequence<T>::GetSubsequence(int startIndex,
                                              int endIndex) const {
    T* array = new T[endIndex - startIndex + 1];
    for (int i = 0; i < endIndex - startIndex + 1; i++) {
        array[i] = items->Get(startIndex + i);
    }
    ArraySequence<T>* newArraySequence =
        new ArraySequence(array, endIndex - startIndex + 1);
    delete[] array;
    return newArraySequence;
}

#endif
