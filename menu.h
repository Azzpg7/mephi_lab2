#include <time.h>

#include "arraystring.h"
#include "cstringio.h"
#include "dequeue.h"
#include "liststring.h"

using namespace std;

void mainMenu() {
  printf("\e[1;1H\e[2J");
  printf("1) Concat 2 strings\n");
  printf("2) Get substring\n");
  printf("3) Find substring\n");
  printf("4) Replace string\n");
  printf("5) Test cases\n");
  printf("6) Exit\n");
}

void testMenu() {
  printf("\e[1;1H\e[2J");
  char c1[] = "aba";
  ListString<char> *ls1 = new ListString<char>(c1, 3);
  printf("ListString 1: ");
  charOutput(ls1);
  ArrayString<char> *as1 = new ArrayString<char>(c1, 3);
  printf("ArrayString 1: ");
  charOutput(as1);
  printf("\n");
  char c2[] = "caba";
  ListString<char> *ls2 = new ListString<char>(c2, 4);
  printf("ListString 2: ");
  charOutput(ls2);
  ArrayString<char> *as2 = new ArrayString<char>(c2, 4);
  printf("ArrayString 2: ");
  charOutput(as2);
  printf("\n");

  clock_t t = clock();
  ListString<char> *ls3 = (ListString<char> *)ls1->Concat(ls2);
  t = clock() - t;
  printf("Result of concatination ListStrings 1 and 2 (ListString 3): ");
  charOutput(ls3);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  t = clock();
  ArrayString<char> *as3 = (ArrayString<char> *)as1->Concat(as2);
  printf("Result of concatination ArrayStrings 1 and 2 (ArrayString 3): ");
  charOutput(as3);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  t = clock();
  printf("\n");

  t = clock();
  ListString<char> *ls4 = (ListString<char> *)ls3->GetSubstring(1, 5);
  t = clock() - t;
  printf(
      "Substring from 1st symbol to the 5th of ListString 3 (ListStirng "
      "4): ");
  charOutput(ls4);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  t = clock();
  ArrayString<char> *as4 = (ArrayString<char> *)as3->GetSubstring(1, 5);
  printf(
      "Substring from 1st symbol to the 5th of ArrayString 3 (ArrayStirng "
      "4): ");
  charOutput(as4);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  t = clock();
  printf("\n");

  char c3[] = "cab";
  ListString<char> *ls5 = new ListString<char>(c3, 3);
  t = clock();
  int ind1 = ls4->Find(ls5);
  t = clock() - t;
  printf("Index of first entry ListString \"cab\" in ListString 4: ");
  printf("%d\n", ind1);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  ArrayString<char> *as5 = new ArrayString<char>(c3, 3);
  t = clock();
  int ind2 = as4->Find(as5);
  t = clock() - t;
  printf("Index of first entry ArrayString \"cab\" in ArrayString 4: ");
  printf("%d\n", ind2);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  printf("\n");

  char c4[] = "haha";
  ListString<char> *ls6 = new ListString<char>(c4, 4);
  t = clock();
  ListString<char> *ls7 = (ListString<char> *)ls4->Replace(ls5, ls6);
  t = clock() - t;
  printf("Replacing \"cab\" with \"haha\" in ListString4: ");
  charOutput(ls7);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  ArrayString<char> *as6 = new ArrayString<char>(c4, 4);
  t = clock();
  ArrayString<char> *as7 = (ArrayString<char> *)as4->Replace(as5, as6);
  t = clock() - t;
  printf("Replacing \"cab\" with \"haha\" in ArrayString4: ");
  charOutput(as7);
  printf("It took %f miliseconds.\n", ((float)t * 1000) / CLOCKS_PER_SEC);
  printf("\n");

  printf("Press any key to continue...");
  scanf("%*c");

  delete ls1;
  delete as1;
  delete ls2;
  delete as2;
  delete ls4;
  delete as4;
  delete ls5;
  delete as5;
  delete ls6;
  delete as6;
  delete ls7;
  delete as7;

  LinkedListDequeue<int> d1;
  d1.SetFirst(1);
  cout << d1.GetFirst() << ' ' << d1.GetLast() << '\n';
  d1.SetFirst(2);
  cout << d1.GetFirst() << ' ' << d1.GetLast() << '\n';
  d1.RemoveFirst();
  cout << d1.GetFirst() << ' ' << d1.GetLast() << '\n';
  printf("Press any key to continue...");
  scanf("%*c");
}

void concatMenu() {
  printf("\e[1;1H\e[2J");
  ListString<char> *ls1 = charInputListString("Input String 1: ");
  ListString<char> *ls2 = charInputListString("Input String 2: ");
  ls1->Concat(ls2);
  printf("Result of concatination is: ");
  charOutput(ls1);
  printf("\n");

  printf("Press any key to continue...");
  scanf("%*c");
  delete ls1;
  delete ls2;
}

void substringMenu() {
  printf("\e[1;1H\e[2J");
  ListString<char> *ls1 = charInputListString("Input String: ");
  int from, to;
  printf("Input first index: ");
  scanf("%d", &from);
  scanf("%*c");
  printf("Input second index: ");
  scanf("%d", &to);
  scanf("%*c");
  ListString<char> *ls2 = (ListString<char> *)ls1->GetSubstring(from, to);
  printf("Result of concatination is: ");
  charOutput(ls2);
  printf("\n");

  printf("Press any key to continue...");
  scanf("%*c");
  delete ls1;
  delete ls2;
}

void findMenu() {
  printf("\e[1;1H\e[2J");
  ListString<char> *ls1 = charInputListString("Input Text: ");
  ListString<char> *ls2 = charInputListString("Input Sample: ");
  int ind = ls1->Find(ls2);
  printf("Index of first entry: ");
  printf("%d", ind);
  printf("\n");

  printf("Press any key to continue...");
  scanf("%*c");
  delete ls1;
  delete ls2;
}

void replaceMenu() {
  printf("\e[1;1H\e[2J");
  ListString<char> *ls1 = charInputListString("Input Text: ");
  ListString<char> *ls2 = charInputListString("Input Sample: ");
  ListString<char> *ls3 = charInputListString("Input Replacement: ");
  ListString<char> *ls4 = (ListString<char> *)ls1->Replace(ls2, ls3);
  int ind = ls1->Find(ls2);
  printf("Result of replacement: ");
  charOutput(ls4);
  printf("\n");

  printf("Press any key to continue...");
  scanf("%*c");
  delete ls1;
  delete ls2;
  delete ls3;
  delete ls4;
}
