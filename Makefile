CC = g++
CFLAGS = -Wall -g

all: lab2

lab2: main.o
		$(CC) $(CFLAGS) main.o -o lab2

main.o: main.cpp dynamicarray.h linkedlist.h sequence.h linkedlistsequence.h arraysequence.h cstring.h liststring.h arraystring.h cstringio.h menu.h
		$(CC) $(CFLAGS) -c main.cpp
