#ifndef LAB2_CSTRINGIO_H
#define LAB2_CSTRINGIO_H

#include <cstring>
#include <iostream>

#include "arraystring.h"
#include "liststring.h"

char *charReadline(const char *s) {
    printf("%s", s);
    char buf[81] = {0};
    char *res = nullptr;
    int len = 0;
    int n = 0;
    do {
        n = scanf("%80[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return nullptr;
            }
        } else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            char *tmp = new char[str_len + 1];
            memcpy(tmp, res, len);
            memcpy(tmp + len, buf, chunk_len);
            delete[] res;
            res = tmp;
            len = str_len;
        } else {
            scanf("%*c");
        }
    } while (n > 0);
    if (len > 0) {
        res[len] = '\0';
    } else {
        res = new char[1];
        res[0] = '\0';
    }
    return res;
}

ListString<char> *charInputListString(const char *s) {
    char *input = charReadline(s);
    int len = strlen(input);
    ListString<char> *str = new ListString<char>(input, len);
    delete[] input;
    return str;
}

ArrayString<char> *charInputArrayString(const char *s) {
    char *input = charReadline(s);
    int len = strlen(input);
    ArrayString<char> *str = new ArrayString<char>(input, len);
    delete[] input;
    return str;
}

void charOutput(const CString<char> *str) {
    int len = str->GetLength();
    for (int i = 0; i < len; i++) {
        cout << str->Get(i);
    }
    cout << '\n';
}

#endif /* LAB2_CSTRINGIO_H */
