#ifndef LAB2_ARRAYSTRING_H
#define LAB2_ARRAYSTRING_H

#include "arraysequence.h"
#include "cstring.h"

template <typename T>
class ArrayString : public CString<T> {
  protected:
    Sequence<T>* GetValue() const override;

  public:
    // Constructors
    ArrayString(T* array, int count);
    ArrayString(Sequence<T>* sequence);
    // Operations
    CString<T>* GetSubstring(int from, int to) const override;
    CString<T>* Replace(CString<T>* old, CString<T>* nw) override;
    // Destructor
    ~ArrayString() {
        delete this->value;
        delete this->hash;
    }
};

template <typename T>
Sequence<T>* ArrayString<T>::GetValue() const {
    return this->value;
}

template <typename T>
ArrayString<T>::ArrayString(T* array, int count) {
    ArraySequence<T>* seq = new ArraySequence<T>(array, count);
    this->value = seq;
    this->len = this->GetValue()->GetLength();
    this->hash = this->CalcHash();
}

template <typename T>
ArrayString<T>::ArrayString(Sequence<T>* sequence) {
    this->value = new ArraySequence<T>(sequence);
    this->len = this->value->GetLength();
    this->hash = this->CalcHash();
}

template <typename T>
CString<T>* ArrayString<T>::GetSubstring(int from, int to) const {
    Sequence<T>* sequence = this->value->GetSubsequence(from, to);
    int len = sequence->GetLength();
    T* array = new T[len];
    for (int i = 0; i < len; i++) {
        array[i] = sequence->Get(i);
    }
    delete sequence;
    ArrayString<T>* substring = new ArrayString<T>(ArrayString<T>(array, len));
    delete[] array;
    return substring;
}

template <typename T>
CString<T>* ArrayString<T>::Replace(CString<T>* old, CString<T>* nw) {
    int ln = old->GetLength();
    long long sampleHash = old->GetHash(0, ln - 1);
    int index = -1;
    for (int i = 0; i < this->len - ln + 1; i++) {
        if (this->GetHash(i, i + ln - 1) == sampleHash) {
            index = i;
            break;
        }
    }
    if (index == -1) {
        return this;
    }
    ArrayString<T>* newString = nullptr;
    if (index > 0) {
        newString = (ArrayString<T>*)this->GetSubstring(0, index - 1);
        newString->Concat(nw);
    } else {
        newString = (ArrayString<T>*)nw->GetSubstring(0, nw->GetLength() - 1);
    }
    if (index + ln < this->len) {
        ArrayString<T>* rstring =
            (ArrayString<T>*)this->GetSubstring(index + ln, this->len - 1);
        newString->Concat(rstring);
        delete rstring;
    }
    return newString;
}

#endif
