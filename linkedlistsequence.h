#ifndef LAB2_LINKEDLISTSEQUENCE_H
#define LAB2_LINKEDLISTSEQUENCE_H

#include "linkedlist.h"
#include "sequence.h"

template <typename T>
class LinkedListSequence : public Sequence<T> {
  private:
    LinkedList<T>* items;

  public:
    // Constructors
    LinkedListSequence() { this->items = new LinkedList<T>; }
    LinkedListSequence(T* items, int count);
    LinkedListSequence(const LinkedList<T>* list);
    // Decomposition
    int GetLength() const override { return this->items->GetLength(); }
    T GetFirst() const override { return this->items->GetFirst(); }
    T GetLast() const override { return this->items->GetLast(); }
    T Get(int index) const override { return this->items->Get(index); }
    Sequence<T>* GetSubsequence(int startIndex, int endIndex) const override;
    // Operations
    void Append(T item) override { this->items->Append(item); }
    void Prepend(T item) override { this->items->Prepend(item); }
    void InsertAt(T item, int index) override;
    Sequence<T>* Concat(Sequence<T>* list) override;
    // Destructor
    ~LinkedListSequence() { delete items; }
};

template <typename T>
LinkedListSequence<T>::LinkedListSequence(T* items, int count) {
    this->items = new LinkedList<T>(items, count);
}

template <typename T>
LinkedListSequence<T>::LinkedListSequence(const LinkedList<T>* list) {
    this->items = new LinkedList<T>(list);
}

template <typename T>
Sequence<T>* LinkedListSequence<T>::GetSubsequence(int startIndex,
                                                   int endIndex) const {
    LinkedList<T>* tmp = items->GetSubList(startIndex, endIndex);
    LinkedListSequence<T>* newLinkedListSequence =
        new LinkedListSequence<T>(tmp);
    delete tmp;
    return newLinkedListSequence;
}

template <typename T>
void LinkedListSequence<T>::InsertAt(T item, int index) {
    this->items->InsertAt(item, index);
}

template <typename T>
Sequence<T>* LinkedListSequence<T>::Concat(Sequence<T>* list) {
    for (int i = 0; i < list->GetLength(); i++) {
        this->items->Append(list->Get(i));
    }
    return this;
}

#endif
