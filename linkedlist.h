#ifndef LAB2_LINKEDLIST_H
#define LAB2_LINKEDLIST_H

#include <memory>
#include <stdexcept>

using namespace std;

template <typename T>
class LinkedList {
  private:
    class Node {
      public:
        shared_ptr<Node> next;
        T value;
        Node() {}
    };

    shared_ptr<Node> start;
    shared_ptr<Node> end;
    int length;

  public:
    // Constructors
    LinkedList(T* items, int count);
    LinkedList();
    LinkedList(const LinkedList<T>* list);
    // Decomposition
    T GetFirst() const;
    T GetLast() const;
    T Get(int index) const;
    LinkedList<T>* GetSubList(int startIndex, int endIndex) const;
    int GetLength() const;
    // Operations
    void Append(T item);
    void Prepend(T item);
    void InsertAt(T item, int index);
    LinkedList<T>* Concat(LinkedList<T>* list);
};

template <typename T>
LinkedList<T>::LinkedList(T* items, int count) {
    this->start = make_shared<Node>();
    this->end = make_shared<Node>();
    this->length = 0;
    this->start->next = end;

    for (int i = 0; i < count; i++) {
        this->Append(items[i]);
    }
}

template <typename T>
LinkedList<T>::LinkedList() {
    this->start = make_shared<Node>();
    this->end = make_shared<Node>();
    this->length = 0;
    this->start->next = end;
}

template <typename T>
LinkedList<T>::LinkedList(const LinkedList<T>* list) {
    this->start = make_shared<Node>();
    this->end = make_shared<Node>();
    this->length = 0;
    this->start->next = end;
    this->length = 0;
    this->start->next = end;
    shared_ptr<Node> listCur = list->start;
    for (int i = 0; i < list->length; i++) {
        listCur = listCur->next;
        this->Append(listCur->value);
    }
}

template <typename T>
T LinkedList<T>::GetFirst() const {
    if (length == 0) {
        throw out_of_range("list is empty");
    }
    return start->next->value;
}

template <typename T>
T LinkedList<T>::GetLast() const {
    if (length == 0) {
        throw out_of_range("list is empty");
    }
    shared_ptr<Node> cur = start->next;
    while (cur->next != end) {
        cur = cur->next;
    }
    return cur->value;
}

template <typename T>
T LinkedList<T>::Get(int index) const {
    if (index >= length || index < 0) {
        throw out_of_range("index out of range");
    }
    shared_ptr<Node> cur = start->next;
    while (index) {
        cur = cur->next;
        index--;
    }
    return cur->value;
}

template <typename T>
LinkedList<T>* LinkedList<T>::GetSubList(int startIndex, int endIndex) const {
    if (startIndex > endIndex) {
        throw invalid_argument("startIndex should be not bigger than endIndex");
    }
    if (startIndex < 0 || endIndex >= length) {
        throw out_of_range("Indexes out of range");
    }
    LinkedList<T>* subList = new LinkedList<T>();
    int curIndex = 0;
    shared_ptr<Node> curNode = start->next;
    while (curIndex <= endIndex) {
        if (curIndex >= startIndex) {
            subList->Append(curNode->value);
        }
        curNode = curNode->next;
        curIndex++;
    }
    return subList;
}

template <typename T>
int LinkedList<T>::GetLength() const {
    return length;
}

template <typename T>
void LinkedList<T>::Append(T item) {
    shared_ptr<Node> newNode = make_shared<Node>();
    newNode->value = item;
    shared_ptr<Node> last = start;
    while (last->next != end) {
        last = last->next;
    }
    last->next = newNode;
    newNode->next = end;
    length++;
}

template <typename T>
void LinkedList<T>::Prepend(T item) {
    shared_ptr<Node> newNode = make_shared<Node>();
    newNode->value = item;
    newNode->next = start->next;
    start->next = newNode;
    length++;
}

template <typename T>
void LinkedList<T>::InsertAt(T item, int index) {
    if (index >= length || index < 0) {
        throw out_of_range("Index out of range");
    }
    shared_ptr<Node> newNode = make_shared<Node>();
    newNode->value = item;
    shared_ptr<Node> cur = start;
    while (index) {
        cur = cur->next;
        index--;
    }
    newNode->next = cur->next;
    cur->next = newNode;
    length++;
}

template <typename T>
LinkedList<T>* LinkedList<T>::Concat(LinkedList<T>* list) {
    shared_ptr<Node> cur = start;
    while (cur->next != end) {
        cur = cur->next;
    }
    cur->next = list->start->next;
    length += list->length;
    end = list->end;
    return this;
}

#endif  // LAB2_LINKEDLIST_H
