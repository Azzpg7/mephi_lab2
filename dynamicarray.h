#ifndef LAB2_DYNAMICARRAY_H
#define LAB2_DYNAMICARRAY_H

#include <memory.h>

#include <stdexcept>

using namespace std;

template <typename T>
class DynamicArray {
  private:
    T* array;
    int size;

  public:
    // Constructors
    DynamicArray();
    DynamicArray(T* items, int count);
    DynamicArray(int size);
    DynamicArray(const DynamicArray<T>* dynamicArray);
    // Decomposition
    T Get(int index) const;
    int GetSize() const;
    void Set(int index, T value);
    void Resize(int newSize);
    // Destructor
    ~DynamicArray() { delete[] array; }
};

template <typename T>
DynamicArray<T>::DynamicArray() {
    array = nullptr;
    size = 0;
}

template <typename T>
DynamicArray<T>::DynamicArray(T* items, int count) {
    array = new T[count];
    memcpy(array, items, count * sizeof(T));
    size = count;
}

template <typename T>
DynamicArray<T>::DynamicArray(int size) {
    T* tmp = new T[size];
    array = tmp;
    this->size = size;
}

template <typename T>
DynamicArray<T>::DynamicArray(const DynamicArray<T>* dynamicArray) {
    size = dynamicArray->GetSize();
    T* tmp = new T[size];
    array = tmp;
    memcpy(array, dynamicArray->array, size * sizeof(T));
}

template <typename T>
T DynamicArray<T>::Get(int index) const {
    if (index < 0 || index >= size) {
        throw out_of_range("index out of range");
    }
    T res = array[index];
    return res;
}

template <typename T>
int DynamicArray<T>::GetSize() const {
    return size;
}

template <typename T>
void DynamicArray<T>::Set(int index, T value) {
    if (index < 0 || index >= size) {
        throw out_of_range("index out of range");
    }
    memcpy(array + index * sizeof(T), &value, sizeof(T));
}

template <typename T>
void DynamicArray<T>::Resize(int newSize) {
    T* tmp = new T[newSize];
    memcpy(tmp, this->array, this->size * sizeof(T));
    array = tmp;
    size = newSize;
}

#endif  // LAB2_DYNAMICARRAY_H
