#include "menu.h"

int main() {
    int c = 0;
    while (c != 6) {
        mainMenu();
        scanf("%d", &c);
        scanf("%*c");
        if (c == 1) {
            concatMenu();
        } else if (c == 2) {
            substringMenu();
        } else if (c == 3) {
            findMenu();
        } else if (c == 4) {
            replaceMenu();
        } else if (c == 5) {
            testMenu();
        }
    }
    return 0;
}
